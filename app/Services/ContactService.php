<?php

namespace App\Services;

use App\Contact;


class ContactService
{
    //public static function findByName($name): Contact
    public static function findByName($name)
    {
        $directory = array(
            'usuario1' => '998495320',
            'usuario2' => '934567891',
            'usuario3' => '945678912',
            //...
        );
        
        return (array_key_exists($name, $directory)) ? $directory[$name] : '';
    }

    public static function validateNumber(string $number): bool
    {
        $lengthValidNumber = 9;
        $firstNumber = 9;
        
        return (strlen($number) == $lengthValidNumber && substr($number,0,1) == $firstNumber) ? true : false;  
    }
    
    public static function validateBody($body)
    {
        return (strlen($body) > 0) ? true : false;
    }
}