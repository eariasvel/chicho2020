<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

class Mobile
{
    protected $provider;

    function __construct(CarrierInterface $provider)
    {
        $this->provider = $provider;
    }

    public function makeCallByName($name = '')
    {
        if( empty($name) ) return;

        $phoneNumber = ContactService::findByName($name);
        
        $validNumber = ContactService::validateNumber($phoneNumber);
        
        if($validNumber)
        {
            $this->provider->dialContact($phoneNumber);

            return $this->provider->makeCall();
        }
        
        return;
    }
    
    public function sendSms($number, $body)
    {
        return (ContactService::validateNumber($number) && ContactService::validateBody($body)) ? $this->provider->sendSms($number, $body) : false;
    }
}

